from setuptools import setup, find_packages

with open('README.rst', encoding='UTF-8') as f:
    readme = f.read()

setup(
    name='rbe',
    version='0.1.0',
    description='Python tool for extracting the boundary of valid pixels from orthomosaics.',
    long_description=readme,
    author='Jesse E. Sprague, Kathleen Mattos, Ashton Regensberg',
    author_email='jesse@omachronlabs.com, kathleenmmattos@gmail.com, Aregensbergdev@gmail.com',
    install_requires=['numpy', 'fiona', 'rasterio', 'geopandas', 'matplotlib'],
    packages=find_packages('src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': [
            'rbe=rbe.cli:main',
        ],
    }
)