from argparse import ArgumentParser

def create_parser():
    parser = ArgumentParser()
    parser.add_argument('-i','--input',
            help='Source folder of orthomosiac tiles to be processed',
            metavar=(''),
            required=True)
    parser.add_argument('-o','--output',
            help='Destination folder for the output file(s)',
            metavar=(''),
            required=True)
    parser.add_argument('-n','--outputFileName', 
            default='outputVectorFile',
            help='Output combined polygon file name; will default to outputVectorFile',
            metavar=('')
            )
    parser.add_argument('-d','--driver',
            choices =["Shapefile", "GeoJSON", "GPKG"],
            default='GPKG',
            help='Output vector driver name; will default to GPKG',
            metavar=('')
            )
    parser.add_argument('-s', '--sieve',
            default=20000,
            help='Pixel threshold for the Rasterio sieve; will default to 20,000',
            metavar=(''))
    return parser

def main():
        from rbe import rbe

        args = create_parser().parse_args()
        rbe.getBorder(args.input, args.output, args.outputFileName, args.driver, args.sieve)
        print("Process Completed")