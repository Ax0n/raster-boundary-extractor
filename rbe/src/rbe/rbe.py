import logging
import sys
import os
import numpy as np
import fiona
import rasterio
from rasterio.features import shapes
from rasterio.features import sieve
import geopandas as gpd
from datetime import datetime, date

logging.basicConfig(stream=sys.stderr, level=logging.INFO)
logger = logging.getLogger('rasterio_polygonize')

# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total: 
        print()


def validPixels(raster_file, vector_file, driver, sieveValue):

    intSieveValue = int(sieveValue)
    with rasterio.Env():

        with rasterio.open(raster_file, mode="r+") as src:
            image = src.read()

            r = src.read(1)
            g = src.read(2)
            b = src.read(3)

            binMask = r + g + b
            binMask = np.where(binMask != 0, 1, binMask)

            sve = sieve(binMask, intSieveValue)

        results = (
            {'properties': {'raster_val': v}, 'geometry': s}
            for i, (s, v)
            in enumerate(
                shapes(sve, transform=src.transform)))

        with fiona.open(
                vector_file, 'w',
                driver=driver,
                crs=src.crs,
                schema={'properties': [('raster_val', 'int')],
                        'geometry': 'Polygon'}) as dst:
            dst.writerecords(results)

    return dst.name


def getBorder(sourceFolder, destinationFolder, megaPolygonName, driver, sieve):

    if driver == 'Shapefile':
        fileExtension = ''
    elif driver == 'GeoJSON':
        fileExtension = '.geojson'
    else :
        fileExtension = '.gpkg'

    timeNow = str(datetime.today().strftime("%b-%d-%Y,%H%M%S%p"))
    subfolderName = ("/individualVectors_" + timeNow + "/")      
    os.mkdir(destinationFolder + subfolderName)
    if os.path.exists(destinationFolder + subfolderName):
        print("Subfolder Created")

    # create a list of all the valid raster file types 
    rasterFileList = []
    for file in os.listdir(sourceFolder):
        if file.endswith('.tif' or '.tiff' or '.jpg' or '.jpeg'):
            rasterFileList.append(file)

    print('Extracting Valid Pixels...')
    l = len(rasterFileList)

    # Initial call to print 0% progress
    printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
    for i, file in enumerate(rasterFileList):
        rasterFilePath = (sourceFolder + "/" + str(file))
        baseFileName = file.split('.')[0]
        
        vectorFileSubfolder = (destinationFolder + subfolderName + str(baseFileName) + fileExtension)
        validPixels(rasterFilePath, vectorFileSubfolder, driver, sieve)
        # Update Progress Bar
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)

    vectorFileList = []
    for file in os.listdir(destinationFolder + subfolderName):
         if file.endswith(fileExtension):
            vectorFileList.append(file)

    if len(vectorFileList) > 1:
        print('Vector Files Created')
        print('Combining Vector Files...')
        firstFile = gpd.read_file(destinationFolder + subfolderName + vectorFileList[0])
        boundries = firstFile[firstFile['raster_val'] == 1]
        baseFileFolder = os.listdir(destinationFolder + subfolderName)   
        firstFile = baseFileFolder[0]
        baseFileName = firstFile.split('.')[0]
        boundries['tilename'] = baseFileName


        l = len(vectorFileList[1:])
        printProgressBar(0, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        for i, file in enumerate(vectorFileList[1:]):
            readableFile = gpd.read_file(destinationFolder + subfolderName + file)
            fileNameWOExtenstion = file.split('.')[0]
            readableFile['tilename'] = fileNameWOExtenstion
            boundries = boundries.append(
                readableFile[readableFile['raster_val'] == 1], ignore_index=True, sort=False)
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)

        if os.path.exists(destinationFolder + "/" + megaPolygonName + fileExtension):
            boundries.to_file(destinationFolder + "/" + megaPolygonName + "_" + timeNow + fileExtension, driver=driver)
            if os.path.exists(destinationFolder + "/" + megaPolygonName + "_" + timeNow + fileExtension):
                print('Combined Vector File Created')
            else:
                print('Error: Combined Vector File Unsuccessful')
        else:
            boundries.to_file(destinationFolder + "/" + megaPolygonName + fileExtension, driver=driver)
            if os.path.exists(destinationFolder + "/" + megaPolygonName + fileExtension):
                print('Combined Vector File Created')
            else:
                print('Error: Combined Vector File Unsuccessful')
    else:
        print("Single File Converted To Vector Format")
