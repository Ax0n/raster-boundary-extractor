# RBE

<!-- <p align="center">
<img src="./assets/RBE_tile_index.png" alt="" style="height: 200px" />
</p> -->


RBE is a CLI Python tool that will extract raster coverage when you have many large contiguous raster tiles with partial no-data segments (i.e., long narrow corridor maps). Raster tiles by nature have to be rectangular and when an irregular area is mapped the raster tiles will fill no-data regions with 0's (or another no data value, sometimes -99999) to create rectangular shapes. If you need to determine the extent of valid pixels this tool can help. 

This tool will interpret any continuous group of pixels with a 0 in all three bands (red, green, blue) as no-data. You can set the `-s` flag to adjust how many continuous pixels represent an area that was not mapped but was included in the raster. This will allow small areas of actual 0 values to exist in the dataset.

### Example of Raster Tiles with No-Data

In the image below, we can see several raster files containing valid data and no-data regions. Creating a raster tile index with a tool like GDAL's [gdaltindex](https://gdal.org/programs/gdaltindex.html) gives us the extent of each tile with the associated tile name, but not the extent of the valid pixels within those tiles.

<p align="center">
<img src="./assets/raster_tile_index.png" alt="" class="center" style="height: 400px" />
</p>



### Example of RBE Output

RBE preserves the tile index data while creating a vector file outlining only the areas of the rasters that contain valid data.

<p align="center">
<!-- <img src="./assets/RBE_tile_index.png" alt="" style="height: 400px" /> -->
<img src="./assets/RBE_valid_pixel_index.png" alt="" style="height: 400px" />
</p>



## Installation

 Universal option for Windows, Mac OS X, Linux, ..., and always provides the latest version
 
### Requirements

- pip
- Python 3.8 or Newer

### Install the Package
1. clone repo
2. cd into rbe/
3. run `pip install dist/rbe-0.1.0-py38-none-any.whl`

## Usage

Example command:
```
    rbe -i ~/path/to/orthomosaics/ -o ~/path/to/vector/ -n validPixelBoundary.geojson -d GeoJSON -s 15000
```

The above example consists of four arguments, the first and sec

```
  -i , --input             Source folder of orthomosiac tiles to be processed                       # required
  -o , --output            Destination folder for the output file(s)                                # required
  -n , --outputFileName    Output combined polygon file name; will default to "outputVectorFile"    # optional
  -d , --driver            Output vector driver name; will default to GPKG                          # optional
  -s , --sieve             Pixel threshold for the Rasterio sieve; will default to 20,000           # optional
```
## Issues

Find a bug?  Let us know by providing...
- python code or command to reproduce the error
- information on your environment
- screenshot of the error

## License

MIT © Axon